To build
 - Install [node](https://nodejs.org/en/) 
 - Run `npm i`
 - Run `npm i -g gulp-cli`
 - Run `gulp dev` to build and watch for changes
 - Run `gulp` to output final build to dist folder