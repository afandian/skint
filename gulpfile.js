const { src, dest, watch, series, parallel } = require('gulp');
const handlebars = require('gulp-compile-handlebars');
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const browsersync = require('browser-sync').create();

function frontPage() {
    return src('./src/pages/index.hbs')
        .pipe(handlebars({}, {batch: ['./src/templates']}))
        .pipe(rename({extname: '.html'}))
        .pipe(dest('./dist'))
        .pipe(browsersync.stream());
}

function pages() {
    return src('./src/pages/*.hbs')
        .pipe(handlebars({}, {batch: ['./src/templates']}))
        .pipe(rename({
            suffix: '/index',
            extname: '.html'
        }))
        .pipe(dest('./dist'))
        .pipe(browsersync.stream());
}

function js(){
    return src('./src/assets/js/*')
        .pipe(dest('./dist/js'))
        .pipe(browsersync.stream());
}

function scss(){
    return src('./src/assets/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(dest('./dist/css'))
        .pipe(browsersync.stream());
}

function css(){
    return src('./src/assets/css/*.css')
        .pipe(dest('./dist/css'))
        .pipe(browsersync.stream());
}

function data(){
    return src('./src/assets/data/**/*')
        .pipe(dest('./dist/data'))
        .pipe(browsersync.stream());
}

function img(){
    return src('./src/assets/img/**/*')
        .pipe(dest('./dist/img'))
        .pipe(browsersync.stream());
}

function watchfiles(){
    watch('src/**/*.hbs', parallel(frontPage, pages));
    watch('src/assets/js/*', js);
    watch('src/assets/css/*.css', css);
    watch('src/assets/scss/*.scss', scss);
    watch('src/assets/data/*', data);
    watch('src/assets/img/**/*', img);
}

function serve(done){
    browsersync.init({
        server: {
            baseDir: './dist',
            serveStaticOptions: {
                extensions: ['html']
            }
        }
    });
    done();
}

const build = parallel(frontPage, pages, js, scss, css, img, data)

exports.dev = series(build, parallel(watchfiles, serve));
exports.default = build;
