FROM node:10 as BUILD

WORKDIR /usr/src/app
RUN mkdir src
COPY ./src ./src
COPY gulpfile.js ./
COPY package*.json ./

RUN npm i
RUN npx gulp

FROM nginx:alpine
COPY --from=build /usr/src/app/dist /usr/share/nginx/html
